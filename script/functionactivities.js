//Act #1
function getSum(num1, num2){
    const sum = num1 + num2;
    console.log(sum);
    return sum;
}
getSum(3, 2);

function getDifference(num1, num2){
    const difference = num1 - num2;
    console.log(difference);
    return difference;
}

getDifference(10, 4);

function getProduct(num1, num2){
    const product = num1 * num2;
    console.log(product);
    return product;
}

getProduct(2, 4);

function getQuotient(num1, num2){
    const quotient = num1 / num2;
    console.log(quotient);
    return quotient;
}

getQuotient(10, 5);
//Act #2

//Volume
//radius is 2
//height is 3

function getRadius(num1, num1){
    const radius = num1 * num1;
    // console.log(radius);
    return radius;

}
getRadius(2);

const squaredRadius = getRadius(2, 2);
// console.log(squaredRadius);


function getBase(squaredRadius, pi){
    const base = squaredRadius * pi;
    // console.log(base);
    return base;

}

getBase(squaredRadius, 3.1416);

const baseArea = getBase(squaredRadius, 3.1416);
// console.log(baseArea);


function getCylinderVolume(baseArea, height){
    const cylinderVolume = baseArea * height;
    console.log(cylinderVolume);
}
getCylinderVolume(baseArea, 3);



//Trapezoid
//base bottom is 5
//base top is 3
//height is 4

function getSumBase(basetop, basebottom){
    const sumBase = basetop + basebottom;
    // console.log(sumBase);
    return sumBase;
}

getSumBase(3, 5);

const sumBaseDone = getSumBase(3, 5);
// console.log(sumBaseDone);

function getQuotientSumBase(sumBaseDone, num3){
    const QoutientSumBase = sumBaseDone/2;
    // console.log(QoutientSumBase);
    return QoutientSumBase;
}

getQuotientSumBase(sumBaseDone, 2);
const QoutientSumBaseDone = getQuotientSumBase(sumBaseDone, 2);
// console.log(QoutientSumBaseDone);

function getTrapezoidArea(QoutientSumBaseDone, heightTrapezoid){
    const TrapezoidArea = QoutientSumBaseDone * heightTrapezoid;
    console.log(TrapezoidArea);

}

getTrapezoidArea(QoutientSumBaseDone, 4);