/**
 * Give at least three reasons why you should learn javascript.
 */

1. Wide range of usage
2. Easy to start with
3. Great career opportunities

/**
 * In two sentences, state in your own words, what is your understanding about Javascript?
 */

1. Functions are objects
2. Automatic type conversions

/**
 * What is a variable? Give at least three examples.
 */

1. JavaScript variables are containers for storing data values.
Examples:
	1. var
	2. let
	3. const

/**
 * What are the data types we talked about today? Give at least two samples each.
 */

===> Here
	Number String 
/**
 * Write the correct data type of the following:
 */

1. "Number" = String
2. 158 =  number
3. pi = error
4. false = boolean
5. 'true' = String
6. 'Not a String' = String

/**
 * What are the arithmetic operators?
 */ addtion
     subtraction
     multiplication
     division
     modulo

/**
 * What is a modulo? 
 */remainder 

/**
 * Interpret the following code
 */

let number = 4;
number += 4;

Q: What is the value of number?

---- 8

let number = 4;
number -= 4;

Q: What is the value of number?

---- 0

let number = 4;
number *= 4;

Q: What is the value of number?

---- 16

let number = 4;
number /= 4;

Q: What is the value of number? 1