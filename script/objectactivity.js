const bankAccount = {
	name: "Brandon Cruz",
	accountNo: "123-456",
	accountBal: 5000,

	showCustomer: function(){
    return "Customer Name: " + bankAccount.name;
    },
    showAccountNumber: function(){
        return "Customer Account Number: " + bankAccount.accountNo;
    },
    showCustomerDetails: function(){
        return "Customer Name: " + bankAccount.name + " / " +"Customer Account Number: " + bankAccount.accountNo + " / " + bankAccount.accountBal;
    },
    showBalance: function(){
        return "Account Balance: " + bankAccount.accountBal;
    },
    deposit: function(amount){
        bankAccount.accountBal += amount;
        return bankAccount.accountBal;
    },
    withdraw: function(amount){
        bankAccount.accountBal -= amount;
        return bankAccount.accountBal;
    }
};


